(function (headroom) {

  var yt_player;
  var play_btn = document.querySelector('.button--video');
  var video = document.querySelector('.video');

  function init_yt(videoId) {

    // Inject YouTube API script
    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";


    tag.defer = true;
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    function onYouTubeIframeAPIReady() {

      yt_player = new YT.Player('youtubeVideo', {
        height: '720',
        width: '1280',
        videoId: videoId,
        playerVars: {
          fs: 0,
          modestbranding: 1,
          color: 'white',
          rel: '0',
          showinfo: '0',
          iv_load_policy: '3',
          origin: window.location.href
        },
        host: 'https://www.youtube.com'

      });
    }
    window.onYouTubeIframeAPIReady = onYouTubeIframeAPIReady;

  }

  function on_keypress(e) {
    e = e || window.event;

    if (e.keyCode == 27) {
      on_closed();
    }
  }

  function spacepress(e) {
    e.preventDefault();

    if (e.keyCode == 32) {
      yt_player.playVideo();

      if (yt_player.getPlayerState() == 1) {
        yt_player.pauseVideo();
      } else {
        yt_player.playVideo();
      }
    }
    return false;
  }

  var is_firstly = true;

  function on_opened() {
    var videoId;

    if (is_firstly) {
      videoId = play_btn.dataset.videoId || '';
      init_yt(videoId);
      is_firstly = false;
    }

    document.body.classList.add('klenovyi-park-page--video-open');

    play_btn.addEventListener("keypress", spacepress);
    play_btn.classList.add('main__button--pressed');
    video.classList.add('video--opened');
    window.addEventListener('keydown', on_keypress, true);

    setTimeout(function () {
      headroom.unpin();
    }, 50);

  }

  function on_closed() {
    headroom.pin();
    yt_player.pauseVideo();
    play_btn.classList.remove('main__button--pressed');
    video.classList.remove('video--opened');

    document.body.classList.remove('klenovyi-park-page--video-open');

    play_btn.removeEventListener("keypress", spacepress);

    window.removeEventListener('keydown', on_keypress, true);
  }

  if (play_btn && video) {
    play_btn.addEventListener('click', function (e) {
      on_opened();
      return false;
    });
    // close video popup after click outside player
    video.addEventListener('click', function (e) {
      if (!e.target.classList.contains('video__wrapper')) {
        on_closed();
      }
    }, false);
  }

})(headroom);
function initMap() {
  var el_lat = document.getElementsByName('lat')[0];
  var el_lng = document.getElementsByName('lng')[0];
  var el_prj_name = document.getElementsByName('project_name')[0];
  var el_map = document.getElementById('map');
  if (el_lat && el_lng && el_map) {
    var myLatLng = {
      lat: parseFloat(el_lat.value),
      lng: parseFloat(el_lng.value)
    };

    var map = new google.maps.Map(el_map, {
      zoom: 18,
      center: myLatLng
    });

    var marker = new google.maps.Marker({
      position: myLatLng,
      map: map,
      title: el_prj_name.value || 'ARS·DIM'
    });
  }
}

function load_map_api() {
  // Inject YouTube API script
  var tag = document.createElement('script');
  tag.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyBAUUmvq5hB8WqthY50Us2OTb0FAdwwNEE&callback=initMap";

  tag.defer = true;
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
}
$(document).ready(function () {

  $('#map').waypoint(function () {

    load_map_api();

    this.enabled = false;
  }, {
    offset: '75%'
  });

});
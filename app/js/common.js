function stage_animation($active_stage_tab) {

	var $counter_num = $active_stage_tab.find('.counter__num');
	var $counter_circle = $active_stage_tab.find('.counter__circle');
	var readiness = $counter_num.data('readiness');

	if ($counter_num.text() != readiness) {

		$counter_num.animateNumber({
			number: readiness || 0
		},
			2000
		);

		$counter_circle
			.attr('stroke-dasharray', readiness + ',100')
			.css('animation', 'circle-chart-fill 2.5s reverse cubic-bezier(0.6, 0.2, 0.1, 1)');
		setTimeout(function () {
			$counter_circle.css('animation', '');
		}, 2500);
	}

}

// animate on scroll by waypoint.js and amimate.css
function animation() {

	//Waypoint.enableAll();

	var $main_title = $('.main__title');

	if ($main_title.length) {
		$main_title.addClass('fadeInUp');
	}

	var elem_projects = document.getElementsByClassName('projects');

	if (elem_projects.length) {

		var waypoint_projects = new Waypoint({

			element: elem_projects[0],
			handler: function () {

				var $circle_chart = $('#projectActiveNum'),
					$circle_done = $('#projectsDoneNum');

				$circle_chart.animateNumber({
					number: $circle_chart.data('count') || 0
				},
					1500
				);

				$circle_done.animateNumber({
					number: $circle_done.data('count') || 0
				},
					3000
				);

				$('.counter__circle')
					.attr('stroke-dasharray', '50,100')
					.css('animation', 'circle-chart-fill 2.5s reverse cubic-bezier(0.6, 0.2, 0.1, 1)');

				this.enabled = false;
			},
			offset: '75%'
		});

	}

	var active_stage_tab = document.querySelector('.stage__content[data-ur-state="enabled"]');

	if (active_stage_tab) {


		var waypoint_stage = new Waypoint({

			element: active_stage_tab,
			handler: function () {

				stage_animation($(active_stage_tab));

				this.enabled = false;
			},
			offset: '75%'
		});

	}

	var $window = $(window);

	// анимация для навигации мобильных устройств

	if ($window.width() < 768) {

		$('.footer').waypoint(function (direction) {

			if (direction == 'down') {

				$(this.element).addClass('fadeOut').removeClass('fadeIn');

			} else if (direction == 'up') {

				$(this.element).addClass('fadeIn').removeClass('fadeOut');

			}

		}, {
			offset: '800px'
		});

	}

	// анимация для десктопа
	else if ($window.width() > 767) {

		$('.animated').each(function () {

			var $el = $(this),
				offset = '75%',
				added_class = 'fadeInUp';

			if ($el.hasClass('done__title')) {
				offset = '55%';
			} else if ($el.hasClass('team__title')) {
				offset = '55%';
			} else if ($el.hasClass('callback-form')) {
				added_class = 'fadeInRight';
				offset = '50%';
			} else if ($el.hasClass('contacts-block') || $el.hasClass('sales-block')) {
				added_class = 'fadeInLeft';
				offset = '85%';
			}

			$el.css('opacity', 0).waypoint(function () {

				$(this.element).addClass(added_class);

				this.enabled = false;
			}, {
				offset: offset
			});

		});

	}

}


$(document).ready(function () {

	svg4everybody({});

	animation();

	// $('.main').parallax({}); // объявлен чз data-

	$('.course__content').each(function () {

		var slides = this.querySelectorAll(".swiper-slide");

		var courseSlider = new Swiper(this.firstElementChild, {
			direction: 'vertical',
			speed: 1000,
			simulateTouch: false,
			preloadImages: false,
			centeredSlides: true,
			initialSlide: Math.max(slides.length - 1, 0),
			lazy: {
				loadPrevNext: true,
				loadedClass: 'fadeIn'
			},
			breakpoints: {
				1024: {
					speed: 300
				}
			},
			pagination: {
				el: '.course .swiper-pagination',
				clickable: true,
				dynamicBullets: true,

				renderBullet: function (index, className) {
					var caption = this.slides[index].getAttribute('data-caption') || '';
					return '<li class="slider__bullet ' + className + '">' + caption + '</li>';
				},
			},
		});
	});
	$('.course__tab').click(function () {
		document.querySelector('.course__content[data-ur-state="enabled"]').firstElementChild.swiper.update();
	});

	$('img.swiper-lazy').css('opacity', 0);

	var historySlider = new Swiper('.history .swiper-container', {
		slidesPerView: 1,
		simulateTouch: false,
		speed: 1000,
		preloadImages: false,
		centeredSlides: true,
		lazy: {
			loadPrevNext: true,
			loadedClass: 'fadeIn'
		},
		breakpoints: {
			1024: {
				speed: 300
			}
		},
		pagination: {
			el: '.history .swiper-pagination',
			clickable: true,
			type: 'bullets',

			dynamicBullets: true,
			hideOnClick: false,

			renderBullet: function (index, className) {
				var caption = this.slides[index].getAttribute('data-caption') || '';
				return '<li class="tab__link ' + className + '">' + caption + '</li>';
			}
		}
	});

	var mainSlider = new Swiper('.main-slider', {
		slidesPerView: 1,
		effect: 'fade',
		simulateTouch: false,
		noSwipingClass: 'swiper-wrapper',
		speed: 1500,
		centeredSlides: false,
		autoplay: {
			delay: 3000,
			disableOnInteraction: false,
		},
	});

	$('.main-slider .to-slide-0').click(function () {
		mainSlider.slideTo(0);
	});
	$('.main-slider .to-slide-1').click(function () {
		mainSlider.slideTo(1);
	});

	var $flats_path = $('.flats__path');
	$flats_path.click(function () {
		select_flat($(this), true);
	});

	if ($(window).width() >= 1024) {
		$flats_path.mouseover(flat_tooltip.show).mouseout(flat_tooltip.hide);
	}


	$('.flats__tab').click(function () {
		var tab_id = $(this).data('ur-tab-id');
		select_flat(
			$('.flats__content[data-ur-tab-id=' + tab_id + '] .flats__path').first()
		);
	});
	select_flat($('.flats__content[data-ur-state=enabled] .flats__path').first());

	$('.flats__content').on('click', '.room__button', function (e) {
		var target = $(this.hash);
		if (target.length) {
			$('html,body').animate({
				scrollTop: target.offset().top
			}, 1500);
			return false;
		}
	});

	$('.stage__tab').click(function () {
		var $btn_tab = $(this);
		var address = $.trim($btn_tab.data('address'));
		var completed = $btn_tab.data('completed');
		var price = $btn_tab.data('price');
		var address_parts = address.split(',');

		stage_animation($('.stage__content[data-ur-state="enabled"]'));

		if (address_parts.length > 1) {
			$('.info__city').text(address_parts[0]);
			$('.info__street').text($.trim(address_parts[1]));
		} else if (address != '') {
			$('.info__city').text(address);
			$('.info__street').text('');
		}
		if (completed && completed != '') {
			$('.info__stage time').text('Введення в експлуатацію ' + completed);
		}
		if (price && price != '') {
			$('.info__cost-price').text(price);
		}
	});

	$('.description__list').scrollTop($(this).height());


	//change all queue tabs by click

	(function () {

		var buttons = document.querySelectorAll('[data-ur-tabs-component="button"]'),
			i = 0,
			len = buttons.length,
			curr_tabId;
		for (; i < len; i++) {

			buttons[i].addEventListener('click', function (e) {
				var j = 0,
					tabId = this.dataset.urTabId,
					button;
				if (curr_tabId != tabId) {
					curr_tabId = tabId;
					for (; j < len; j++) {
						button = buttons[j];
						if (button != this && button.dataset.urTabId == tabId) {
							button.click();
						}
					}
				}
			});

		}


	})();

	//.

});



// lazy loading
window.lazySizesConfig = window.lazySizesConfig || {};
window.lazySizesConfig.requireJs = function (modules, cb) {
	window.require(modules, cb);
};

var lazy = function lazy() {
	document.addEventListener('lazyloaded', function (e) {
		e.target.parentNode.classList.add('image-loaded');
		e.target.parentNode.classList.remove('loading');
	});
}

lazy();

// macy grid
var elems_project_bg = document.querySelectorAll('.project-block__bg');

if (elems_project_bg.length) {

	var macy = Macy({
		container: '.grid',
		trueOrder: false,
		waitForImages: false,
		margin: 50,
		columns: 1,
		mobileFirst: true,
		useOwnImageLoader: true,
		breakAt: {
			480: 2,
			1024: 3
		}
	});

	[].forEach.call(elems_project_bg, function (img) {
		img.onload = function () {
			macy.recalculate(true);
			Waypoint.refreshAll();
			//img.removeAttribute('data-src');
		};
	});

	macy.on(macy.constants.EVENT_RESIZE, function (ctx) {
		Waypoint.refreshAll();
	});

}

// headroom
var hideHeader = document.querySelector('header');
var headroom = new Headroom(hideHeader, {
	'offset': 300,
	'tolerance': 15
});
headroom.init();
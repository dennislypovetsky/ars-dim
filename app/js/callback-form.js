$(document).ready(function(){

  var exception_msg={
    "name":"Перевірте ім'я",
    "phone":"Телефон невірний",
    "blank":"Заповніть це поле"
  };


  var $callback_form=$('.callback-form');
  var $popup=$('.callback-form__pop-up');

  var $name=$callback_form.find('#name');
  var $phone=$callback_form.find('#phone');
  var $question=$callback_form.find('#question');

  var r_name = /^\D{3,}$/;
  var r_except_nums = /\D/g;

  // $callback_form[0].noValidate = true;

  function form_popup(message){
      $popup.addClass('active').html('<span>'+message+'</span>');
      setTimeout(function(){
          //$popup.fadeOut( 'slow',function(){
              $popup.removeClass('active').prop('style','');
          //} );
      },2000);
  }
  if($popup.hasClass('active')){
    setTimeout(function(){
        //$popup.fadeOut( 'slow',function(){
            $popup.removeClass('active').prop('style','');
        //} );
    },5000);
  }

  function objectifyForm(formArray) {//serialize data function
      var returnArray = {};
      for (var i = 0; i < formArray.length; i++){
        returnArray[formArray[i]['name']] = formArray[i]['value'];
      }
      return returnArray;
  }
  function checkNativeValidation(field){
    // is native browser validation available?
    return (typeof field.willValidate !== "undefined");
  }

  var check_name=function(){
      var val=$name.val();
      if(val==''){
        msg=exception_msg['blank'];
        $name.addClass('invalid');
      }
      else if( !r_name.test( val ) ) {
        var msg=exception_msg['name'];
        $name.addClass('invalid');
      }
      else{
        var msg="";
        $name.removeClass('invalid');
      }
      $name[0].setCustomValidity(msg);
      return msg;
  };
  var check_phone=function(){
      var phone=$phone.val().replace(r_except_nums,''), msg;
      if(phone.length==12){
        msg="";
        $phone.removeClass('invalid');
      }
      else{
        msg=exception_msg['phone'];
        $phone.addClass('invalid');
      }
      //var msg=(phone.length!=12) ? exception_msg['phone'] : "";
      $phone[0].setCustomValidity(msg);

      return msg;
  };
  $name.on('invalid input',check_name);

  $phone.on('change',check_phone);

  // jquery.maskedinput
  $phone.mask('+389 99 999 99 99', { autoclear: false, completed: check_phone});

  // textarea-autosize
  $('.callback-textarea').textareaAutoSize();

  $callback_form.submit(function(e){
    e.preventDefault();

    check_name();
    check_phone();

    var phone=$phone.val().replace(r_except_nums,'');
    var data=objectifyForm($callback_form.serializeArray());
    data.phone='+'+phone;
    $.ajax({
      url:$callback_form.attr('action'),
      type:'POST',
      data:data
      //data:$callback_form.serialize()
    })
    .done(function(result){
      form_popup(result.message);
      $callback_form.trigger("reset");
    })
    .fail(function(jqXHR, textStatus){
      if(jqXHR.responseJSON){
          var errors=jqXHR.responseJSON.errors;
          if(errors){
              var ul='<ul>';
              Object.keys(errors).map(function(key, index) {
                  ul+= '<li>'+errors[key]+'</li>';
              });
              ul+='</ul>';
              form_popup(ul);
          }
          else{
            form_popup('Помилка при відправленні. Спробуйте ще раз');
          }
          //$callback_form.trigger("reset");
      }

    });
    return false;
  });

});

var load_flat = function () {
	var loading_url;

	return function (url, $container, cb) {
		if (url !== loading_url) {
			loading_url = url;
			$.get(url)
				.done(function (flat) {
					if (flat['alert-type'] && flat['alert-type'] == 'success') {
						$container.append(flat.html);
						if (cb) {
							cb();
						}
					}
				});
		}
	}
}();

var flatSelected = new Event("flatSelected", {});

var select_flat = function () {
	var $selected_path, $selected_flat,
		cls_flat_active = 'room__wrapper--active',
		cls_path_active = 'flats__path--active';

	return function ($path, is_path_click) {
		var $flat,
			flat_id = $path.data('flat-id'),
			$flat_parent;
		if (!$path.hasClass(cls_path_active)) {
			$flat = $(".room__wrapper[data-flat-id=" + flat_id + "]");
			if (!$flat.length) {
				return load_flat(
					$path.data('flat-url'),
					$path.closest('.flats__holder').find('.flats__room'),
					select_flat.bind(this, $path, is_path_click)
				);
			}

			if ($selected_flat && $selected_flat.length) {
				$selected_flat.removeClass(cls_flat_active);
			}
			if ($selected_path && $selected_path.length) {
				$selected_path.removeClass(cls_path_active);
			}
			$path.addClass(cls_path_active);

			if ($(window).width() < 700 && is_path_click) {

				$('html,body').animate({
					scrollTop: $flat.offset().top
				}, 500);
			}

			if (is_path_click) {
				$flat.addClass(cls_flat_active);
			} else {
				$flat.addClass(cls_flat_active);
			}

			$selected_flat = $flat;
			$selected_path = $path;
			window.dispatchEvent(flatSelected);
		}
	};
}();

var flat_tooltip = function (flats) {

	var curr_tooltip,
		cls_tooltip_open = 'information--open',
		corner_x = 30,
		corner_y = 20,
		top = 0,
		left = 0;

	function setPosition() {
		setTimeout(function() {
			top = flats.offsetTop || 0;
			left = flats.firstElementChild.offsetLeft || 0;
		}, 100);
	}

	if (flats) {
		setPosition();
		window.addEventListener('resize', setPosition, true);
		window.addEventListener('flatSelected', setPosition, true);
	}

	function show(e) {
		var path = this,
			flat_id = path.dataset.flatId,
			tooltip = document.querySelector('.information[data-flat-id="' + flat_id + '"]'),
			tooltip_rect = tooltip.getBoundingClientRect();

		corner_y = -tooltip_rect.height - 20;

		tooltip.style.left = (e.pageX - left + corner_x) + 'px';
		tooltip.style.top = (e.pageY - top + corner_y) + 'px';
		tooltip.classList.add(cls_tooltip_open);

		curr_tooltip = tooltip;

		$(document).on('mousemove', move);
	}

	function move(e) {
		curr_tooltip.style.left = (e.pageX - left + corner_x) + 'px';
		curr_tooltip.style.top = (e.pageY - top + corner_y) + 'px';
	}

	function hide() {
		if (curr_tooltip) {
			curr_tooltip.classList.remove(cls_tooltip_open);
			$curr_tooltip = null;
			$(document).off('mousemove', move);
		}
	}

	return {
		show: show,
		hide: hide,
		move: move
	};
}(document.querySelector('.flats'));

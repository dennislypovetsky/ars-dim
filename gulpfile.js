var gulp = require('gulp'),
	gutil = require('gulp-util'),
	pug = require('gulp-pug'),
	fs = require('fs'),
	sass = require('gulp-sass'),
	browserSync = require('browser-sync'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	sourcemaps = require('gulp-sourcemaps'),
	cleanCSS = require('gulp-clean-css'),
	rename = require('gulp-rename'),
	del = require('del'),
	responsive = require('gulp-responsive'),
	imagemin = require('gulp-imagemin'),
	mozjpeg = require('imagemin-mozjpeg'),
	pngquant = require('imagemin-pngquant'),
	svg = require('gulp-svgmin'),
	svgSprite = require('gulp-svg-sprite'),
	cheerio = require('gulp-cheerio'),
	replace = require('gulp-replace'),
	cache = require('gulp-cache'),
	autoprefixer = require('gulp-autoprefixer'),
	ftp = require('vinyl-ftp'),
	notify = require('gulp-notify'),
	rsync = require('gulp-rsync');

// Пользовательские скрипты проекта


gulp.task('gather:js-scripts', function () {
	return gulp.src([
		'app/js/flat.js',
		'app/js/map.js',
		'app/js/common.js',
		'app/js/callback-form.js',
		'app/js/popup-video.js'
	])
		.pipe(sourcemaps.init())
		.pipe(concat('common.min.js'))
		//.pipe(uglify())
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest('app/js'));
});

gulp.task('gather:js-libs', function () {

	return gulp.src([
		'app/libs/jquery/dist/jquery.min.js',
		'node_modules/lazysizes/plugins/unveilhooks/ls.unveilhooks.min.js',
		'node_modules/lazysizes/lazysizes.min.js',
		'app/libs/parallax/dist/parallax.min.js',
		'node_modules/svg4everybody/dist/svg4everybody.min.js',
		'node_modules/headroom.js/dist/headroom.min.js',
		'node_modules/swiper/dist/js/swiper.min.js',
		'node_modules/waypoints/lib/jquery.waypoints.min.js',
		'app/libs/jquery.animateNumber/jquery.animateNumber.min.js',
		'node_modules/jquery.maskedinput/src/jquery.maskedinput.js',
		'node_modules/textarea-autosize/dist/jquery.textarea_autosize.js',
		'node_modules/macy/dist/macy.js',
		'app/libs/uranium/uranium-pretty.js'
	])
		.pipe(sourcemaps.init())
		.pipe(concat('libs.js'))
		.pipe(gulp.dest('app/js'))
		.pipe(rename('libs.min.js'))
		.pipe(uglify()) // Минимизировать весь js (на выбор)
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest('app/js'));
});

gulp.task('js', ['gather:js-libs', 'gather:js-scripts'], function () {
	return gulp.src([
		'app/js/libs.min.js',
		'app/js/common.min.js' // Всегда в конце
	])
		.pipe(sourcemaps.init())
		.pipe(concat('scripts.js'))
		.pipe(gulp.dest('app/js'))
		.pipe(rename('scripts.min.js'))
		.pipe(uglify()) // Минимизировать весь js (на выбор)
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest('app/js'))
		.pipe(browserSync.reload({
			stream: true
		}));
});

gulp.task('pug', function () {
	return gulp.src('app/pug/pages/*.pug')
		.pipe(pug({
			locals: {
				data: JSON.parse(fs.readFileSync('app/data/data.json', 'utf8')),
				klenova: JSON.parse(fs.readFileSync('app/data/klenova.json', 'utf8')),
				btn: JSON.parse(fs.readFileSync('app/data/buttons.json', 'utf8'))
			},
			pretty: true
		}))
		.on('error', notify.onError(function (error) {
			return {
				title: 'Pug',
				message: error.message
			};
		}))
		.pipe(gulp.dest('app'))
		.on('end', browserSync.reload);
});

gulp.task('sass', function () {
	return gulp.src('app/sass/**/*.sass')
		//.pipe(sourcemaps.init())
		.pipe(sass({
			outputStyle: 'expand'
		}).on('error', notify.onError()))
		.pipe(rename({
			suffix: '.min',
			prefix: ''
		}))
		.pipe(autoprefixer(['last 3 versions']))
		.pipe(cleanCSS()) // Опционально, закомментировать при отладке
		//.pipe(sourcemaps.write())
		.pipe(gulp.dest('app/css'))
		.pipe(browserSync.reload({
			stream: true
		}));
});

gulp.task('watch', ['pug', 'svg', 'sass', 'js', 'browser-sync'], function () {
	gulp.watch('app/pug/**/*.pug', ['pug']);
	gulp.watch('app/data/**/*.json', ['pug']);
	gulp.watch('app/img/svg/**/*.svg', ['svg']);
	gulp.watch('app/sass/**/*.sass', ['sass']);
	gulp.watch(['libs/**/*.js', 'app/js/popup-video.js', 'app/js/map.js', 'app/js/flat.js', 'app/js/common.js'], ['js']);
	gulp.watch('app/*.html', browserSync.reload);
});

gulp.task('images', function () {
	return gulp.src('app/data-img/flats/*.{jpg,jpeg,png}')
		.pipe(responsive({
			'*': [{
				width: 1000,
				rename: {
					suffix: '_1000'
				},
			}, {
				width: 588,
				rename: {
					suffix: '_588'
				},
			}, {
				width: 294,
				rename: {
					suffix: '_294'
				},
			}],
		}, {
			// format: 'jpg',
			progressive: true,
			withMetadata: false,
			withoutEnlargement: true,
			errorOnEnlargement: false,
			errorOnUnusedConfig: false,
			errorOnUnusedImage: false
		}))
		.pipe(gulp.dest('app/data-img/'));
});

// Заготовчка
// '*': [{
// 	width: 1000,
// 	rename: {
// 		suffix: '_1000',
// 		extname: '.jpg'
// 	},
// }, {
// 	width: 588,
// 	rename: {
// 		suffix: '_588',
// 		extname: '.jpg'
// 	},
// }, {
// 	width: 294,
// 	rename: {
// 		suffix: '_294',
// 		extname: '.jpg'
// 	},
// }],


// '*': [{
// 	width: 1920,
// 	rename: {
// 		suffix: '_1920',
// 		extname: '.jpg'
// 	},
// }, {
// 	width: 1280,
// 	rename: {
// 		suffix: '_1280',
// 		extname: '.jpg'
// 	},
// }, {
// 	width: 640,
// 	rename: {
// 		suffix: '_640',
// 		extname: '.jpg'
// 	},
// }, {
// 	width: 320,
// 	rename: {
// 		suffix: '_320',
// 		extname: '.jpg'
// 	},
// }],

gulp.task('imagemin', function () {
	return gulp.src(['app/img/**/*.{png,jpg,svg}', '!app/img/svg/**/*.svg'])
		.pipe(cache(imagemin([
			mozjpeg({
				quality: 65
			}),
			pngquant({
				quality: 65,
				speed: 1
			})
		])))
		.pipe(gulp.dest('dist/img'));
});
gulp.task('svg', function () {
	return gulp.src('app/img/svg/*.svg')
		.pipe(cache(svg({
			js2svg: {
				pretty: true
			}
		})))
		.pipe(cheerio({
			run: function ($) {
				$('[fill]').removeAttr('fill');
				$('[stroke]').removeAttr('stroke');
				$('[style]').removeAttr('style');
			},
			parserOptions: {
				xmlMode: true
			}
		}))
		.pipe(replace('&gt;', '>'))
		.pipe(svgSprite({
			mode: {
				symbol: {
					sprite: 'sprite.svg'
				}
			}
		}))
		.pipe(gulp.dest('app/img/svg'));
});

gulp.task('browser-sync', function () {
	browserSync({
		server: {
			baseDir: 'app'
		},
		notify: false,
		open: false,
		// host: "192.168.0.104",
		// tunnel: true,
		// tunnel: '//ars-dim.localtunnel.me/', //Demonstration page: http://projectmane.localtunnel.me
	});
});

gulp.task('build', ['removedist', 'imagemin', 'svg', 'sass', 'js'], function () {

	var buildFiles = gulp.src([
		'app/*.html',
		'app/.htaccess',
	]).pipe(gulp.dest('dist'));

	var buildCss = gulp.src([
		'app/css/main.min.css',
	]).pipe(gulp.dest('dist/css'));

	var buildJs = gulp.src([
		'app/js/scripts.min.js',
	]).pipe(gulp.dest('dist/js'));

	var buildFonts = gulp.src([
		'app/fonts/**/*',
	]).pipe(gulp.dest('dist/fonts'));

});

gulp.task('deploy', function () {

	var conn = ftp.create({
		host: '',
		user: '',
		password: '',
		parallel: 10,
		log: gutil.log
	});

	var globs = [
		'dist/**',
		'dist/.htaccess',
	];
	return gulp.src(globs, {
		buffer: false
	})
		.pipe(conn.dest(''));

});

gulp.task('rsync', function () {
	return gulp.src('dist/**')
		.pipe(rsync({
			root: 'dist/',
			hostname: 'username@yousite.com',
			destination: 'yousite/public_html/',
			// include: ['*.htaccess'], // Скрытые файлы, которые необходимо включить в деплой
			recursive: true,
			archive: true,
			silent: false,
			compress: true
		}));
});

gulp.task('removedist', function () {
	return del.sync('dist');
});
gulp.task('clearcache', function () {
	return cache.clearAll();
});

gulp.task('default', ['watch']);